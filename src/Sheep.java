public class Sheep {

    enum Animal {sheep, goat}


    public static void main(String[] param) {
        // for debugging
    }

    public static void reorder(Animal[] animals) {
        // Creating new empty Array and some variables
        Animal[] new_animals = new Animal[animals.length];
        int indexGoat = 0;
        int indexSheep = 0;

        // Here we count sheeps and goats and after adding them to the new Array in this for loop
        for (Animal animal : animals) {

            // Here if animal is equal Animal.goat, we add them from the start of new Array
            if (animal.equals(Animal.goat)) {
                new_animals[indexGoat] = animal;
                indexGoat++;

                // If animal is not equal Animal.goat, we add them from the end of new Array
            } else {
                new_animals[new_animals.length - indexSheep - 1] = animal;
                indexSheep++;
            }
        }
//         Here we replace animals from the original array with an animals from new created array
        for (int i = 0; i < animals.length; i++) {
            animals[i] = new_animals[i];
        }
    }
}